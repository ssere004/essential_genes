import pandas as pd
from Bio import SeqIO
import pickle
import re

def parse_annot(address):
    annot_df = pd.read_table(address, sep='\t', comment='#')
    annot_df.columns = ['chr', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
    annot_df['ID'] = annot_df['attributes'].str.extract(r'ID=([^;]+)')
    annot_df['chr'] = annot_df['chr'].apply(str.lower)
    return annot_df

def parse_fasta(address):
    recs = SeqIO.parse(address, "fasta")
    sequences = {}
    for chro in recs:
        sequences[chro.id.lower()] = chro.seq
    for i in sequences.keys():
        sequences[i] = sequences[i].upper()
    return sequences

def parse_atac_peak(address):
    df = pd.read_csv(address, sep='\t')
    df.columns = ['ID', 'chr', 'start', 'end', 'T10', 'T20', 'T30', 'T40']
    df.chr = df['chr'].str.lower()
    return df

def parse_proteins_seq(add):
    with open(add, 'r') as file:
        input_text = file.read()
    parsed_pairs = []
    pattern = re.compile(r'>tr\|\w+\|(\w+)_\w+ .* GN=(\w+) ')
    matches = pattern.findall(input_text)
    for match in matches:
        parsed_pairs.append(match)
    return pd.DataFrame(parsed_pairs, columns=['protein', 'gene'])




