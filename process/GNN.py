import configs
import io_manager as iom
import pandas as pd
import configs
from calc import converter
import torch
from torch_geometric.data import Data, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool
import pandas as pd
import numpy as np
import preprocess.epigenetic_feature_prep as efp

gene_protein_df = iom.parse_proteins_seq(configs.pf_protein_seqs)
replacement_dict = gene_protein_df.set_index('protein')['gene'].to_dict()
ppi_df = pd.read_csv(configs.pf_ppi, sep='\t')
ppi_df['gene1'] = ppi_df['protein1'].map(replacement_dict)
ppi_df['gene2'] = ppi_df['protein2'].map(replacement_dict)
ppi_df = ppi_df.dropna().reset_index(drop=True)
ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]

annot_df = iom.parse_annot(configs.annot)
annot_df = annot_df[annot_df.type == 'gene']
atac_df = iom.parse_atac_peak(configs.atac_peak_add)
assembly = iom.parse_fasta(configs.assembly)

feature_seqs = efp.get_attac_seq_features(atac_df, assembly) + efp.get_h3k9ac_features(assembly, configs.H3K9ac_peaks)
X_df = pd.DataFrame({'chr': annot_df['chr'],
                     'start': annot_df['start'],
                     'end': annot_df['end'],
                     'strand': annot_df['strand'],
                     'ID': annot_df['ID']})
bin_num = 10
for i, ftr_seq in enumerate(feature_seqs):
    X_df['feature_' + str(i)] = X_df.apply(lambda row: efp.calc_featers(row, ftr_seq, bin_num, reg='us'), axis=1)

feature_columns = [col for col in X_df.columns if 'feature_' in col]
gene_features_dict = X_df.set_index('ID')[feature_columns].apply(lambda row: [item for sublist in row for item in sublist], axis=1).to_dict()

ppi_df['combined_score'] = (ppi_df['combined_score'] - ppi_df['combined_score'].min())/(ppi_df['combined_score'].max() - ppi_df['combined_score'].min())

#making a dictionary that converts gene names to their index, The size of whole dictionary is the number of all available genes
gene_to_idx = {gene: idx for idx, gene in enumerate(np.unique(ppi_df[['gene1', 'gene2']].values.flatten()))}
#making a tensor with shape of [2, edge_number], that for each [:,i] it contains two gene indexes (computed in the line above) that they are connected in the graph
edge_index = torch.tensor([[gene_to_idx[gene1], gene_to_idx[gene2]] for gene1, gene2 in zip(ppi_df['gene1'], ppi_df['gene2'])], dtype=torch.long).t().contiguous()
#Making the edge features basd on the ppi_df, see that the zip(ppi_df['gene1'], ppi_df['gene2']) in the line above is in the same order of ppi_df['combined_score'].values in the line below
edge_attr = torch.tensor(ppi_df['combined_score'].values, dtype=torch.float).unsqueeze(1)
#edge_attr[:, 0] = 1
# Initialize node_features tensor with zeros
node_features = torch.zeros(len(gene_to_idx), len(feature_columns) * bin_num)

# Assign the specific vector to each node
for gene, idx in gene_to_idx.items():
    if gene in gene_features_dict:
        node_features[idx] = torch.tensor(gene_features_dict[gene])

num_genes = len(gene_to_idx)  # Total number of unique genes
embedding_dim = len(feature_columns) * bin_num  # Or any other dimension you choose for the embeddings
output_dim = 1  # Assuming we're predicting a single value for essentiality
node_idx = torch.arange(0, num_genes, dtype=torch.long)
ess_g_dict = pd.Series(ess_g_df.MIS.values, index=ess_g_df.Gene_ID).to_dict()
essentiality_index = torch.tensor([ess_g_dict.get(gene, 0) for gene in gene_to_idx.keys()], dtype=torch.float)

num_genes = len(gene_to_idx)
essentiality_index = torch.zeros(num_genes)
essentiality_binary = torch.zeros(num_genes)
threshold = 0.5

# Update essentiality_index with values from ess_g_df
for _, row in ess_g_df.iterrows():
    gene = row['Gene_ID']
    mis_value = row['MIS']
    if gene in gene_to_idx.keys():
        index = gene_to_idx[gene]
        essentiality_index[index] = mis_value
        essentiality_binary[index] = 1 if mis_value > threshold else 0

#make dummy dataset easy to predict
for idx in range(len(essentiality_binary)):
    if essentiality_binary[idx] == 1:
        node_features[idx] = (node_features[idx] * 0) + 1
    else:
        node_features[idx] = node_features[idx] * 0


torch.manual_seed(0)  # Use a fixed seed for reproducibility
num_nodes = essentiality_binary.size(0)
indices = torch.randperm(num_nodes)
train_size = int(0.8 * num_nodes)
test_size = num_nodes - train_size
train_mask = torch.zeros(num_nodes, dtype=torch.bool)
test_mask = torch.zeros(num_nodes, dtype=torch.bool)
train_mask[indices[:train_size]] = True
test_mask[indices[train_size:]] = True


import torch
import torch.nn as nn
import torch.nn.functional as F

class GeneGNN(torch.nn.Module):
    def __init__(self, embedding_dim, output_dim):
        super(GeneGNN, self).__init__()
        self.conv1 = GCNConv(embedding_dim, 16)  # Use embedding_dim as input feature size
        self.conv2 = GCNConv(16, output_dim)
    def forward(self, x, edge_index, edge_attr):
        x = F.relu(self.conv1(x, edge_index, edge_weight=edge_attr.view(-1)))
        x = self.conv2(x, edge_index, edge_weight=edge_attr.view(-1))
        return x.squeeze()

class GeneGNNBinary(torch.nn.Module):
    def __init__(self, embedding_dim):
        super(GeneGNNBinary, self).__init__()
        self.conv1 = GCNConv(embedding_dim, 16)
        self.conv2 = GCNConv(16, 1)  # Assuming output_dim is 1 for binary classification
        self.sigmoid = nn.Sigmoid()
    def forward(self, x, edge_index, edge_attr):
        x = F.relu(self.conv1(x, edge_index, edge_weight=edge_attr.view(-1)))
        x = self.conv2(x, edge_index, edge_weight=edge_attr.view(-1))
        x = self.sigmoid(x).squeeze()
        return x

import torch
from torch.nn import Linear
import torch.nn.functional as F
class MLP(torch.nn.Module):
    def __init__(self, hidden_channels):
        super().__init__()
        torch.manual_seed(12345)
        self.lin1 = Linear(embedding_dim, hidden_channels)
        self.lin2 = Linear(hidden_channels, 1)
    def forward(self, x):
        x = self.lin1(x)
        x = x.relu()
        #x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin2(x)
        x = x.relu()
        return x

data = Data(x=node_features, edge_index=edge_index, edge_attr=edge_attr, y=essentiality_binary)

data.train_mask = train_mask
data.test_mask = test_mask
data.y = essentiality_binary

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = 'cuda'
model = GeneGNNBinary(embedding_dim=embedding_dim)
#model = MLP(hidden_channels=16)
model.to(device)
data = data.to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
criterion = nn.BCELoss()
model.train()

for epoch in range(400):  # Number of epochs
    optimizer.zero_grad()
    out = model(node_features.to(device), data.edge_index.to(device), data.edge_attr.to(device))
    #out = model(node_features.to(device))
    loss = criterion(out[data.train_mask], data.y[data.train_mask])
    #loss = criterion(out[data.train_mask][:, 0], data.y[data.train_mask])
    loss.backward()
    optimizer.step()
    if epoch % 10 == 0:
        print(f'Epoch {epoch}: Training loss = {loss.item()}')

model.eval()
with torch.no_grad():
    out = model(node_features.to(device), data.edge_index.to(device), data.edge_attr.to(device))
    #out = model(node_features.to(device))
    # Convert probabilities to binary predictions for the test set
    preds = (out > threshold).float()  # Threshold at 0.5
    correct = (preds[data.test_mask] == data.y[data.test_mask]).float().sum()
    #correct = torch.sum(preds[data.test_mask] == data.y[data.test_mask]).item()# Correct predictions in the test set
    accuracy = correct / data.test_mask.sum()  # Test accuracy
    print(f'Test Accuracy: {accuracy.item() * 100:.2f}%')



# Initialize the model
# Initialize the model without input_dim
# model = GeneGNN(num_genes=num_genes, embedding_dim=embedding_dim, output_dim=output_dim)
# optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
# criterion = torch.nn.MSELoss()
#
# model.train()
# for epoch in range(40):  # Number of epochs
#     optimizer.zero_grad()
#     # Pass node indices for embedding lookup and other data as before
#     out = model(node_idx, data.edge_index, data.edge_attr)
#     loss = criterion(out, data.y)
#     loss.backward()
#     optimizer.step()
#     print(f'Epoch {epoch+1}, Loss: {loss.item()}')

#
# model.eval()  # Set the model to evaluation mode
#
# with torch.no_grad():  # Inference mode, no need to calculate gradients
#     predictions = model(node_idx, edge_index, edge_attr)
#
# from sklearn.metrics import mean_squared_error, r2_score
# mse = mean_squared_error(essentiality_index.numpy(), predictions.numpy())
# r2 = r2_score(essentiality_index.numpy(), predictions.numpy())
# print(f"MSE: {mse}, R^2: {r2}")



