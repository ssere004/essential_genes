import pandas as pd
import configs as configs
import numpy as np
import torch
from torch_geometric.data import Data
import torch.nn as nn
from torch.nn import Linear, Module
import torch.nn.functional as F
from torch_geometric.nn import GCNConv

yeast_config = configs.yeast_config
ess_g_df = pd.read_csv(yeast_config['ess_g'], sep=',')
ppi_df = pd.read_csv(yeast_config['ppi'], sep='\t')
annot_df = pd.read_csv(yeast_config['annot'], sep='\t', comment='#', header=None, names=['chr', 'source', 'type', 'start', 'end', 'a', 'strand', 'b', 'attr'])
annot_df = annot_df[annot_df.type == 'gene']
ppi_df['combined_score'] = (ppi_df['combined_score'] - ppi_df['combined_score'].min())/(ppi_df['combined_score'].max() - ppi_df['combined_score'].min())
gene_to_idx = {gene: idx for idx, gene in enumerate(np.unique(ppi_df[['gene1', 'gene2']].values.flatten()))}
edge_index = torch.tensor([[gene_to_idx[gene1], gene_to_idx[gene2]] for gene1, gene2 in zip(ppi_df['gene1'], ppi_df['gene2'])], dtype=torch.long).t().contiguous()
edge_attr = torch.tensor(ppi_df['combined_score'].values, dtype=torch.float).unsqueeze(1)
edge_attr[:, 0] = 1
node_features = torch.zeros(len(gene_to_idx), 100)

#load data from the gene_name_converter and then convert the names of ess_g_df which are in SGD format to systematic format.
gene_names_df = pd.read_csv(yeast_config['gene_name_converter'], sep='\t', names=['0', 'systematic', 'og', 'standard', 'attr'])
replacement_dict = gene_names_df.set_index('standard')['systematic'].to_dict()
ess_g_df['gene'] = ess_g_df['gene'].map(replacement_dict)

#preparing the node labels
essentiality_binary = torch.zeros(len(gene_to_idx)) - 1
for _, row in ess_g_df.iterrows():
    gene = row['gene']
    if gene in gene_to_idx.keys():
        index = gene_to_idx[gene]
        essentiality_binary[index] = 1 if row['essentiality'] == 'E' else 0

data = Data(x=node_features, edge_index=edge_index, edge_attr=edge_attr, y=essentiality_binary)


torch.manual_seed(0)  # Use a fixed seed for reproducibility
num_nodes = data.x.shape[0]
indices = torch.randperm(num_nodes)
train_size = int(0.8 * num_nodes)  # 80% for training
val_size = int(0.1 * num_nodes)    # 10% for validation
data.train_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.val_mask = torch.zeros(num_nodes, dtype=torch.bool)  # Validation mask
data.test_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.train_mask[indices[:train_size]] = True
data.val_mask[indices[train_size:train_size + val_size]] = True  # Validation mask
data.test_mask[indices[train_size + val_size:]] = True

labeled_mask = data.y != -1  # Define a mask for labeled nodes

# Update masks to consider only labeled data
data.train_mask = data.train_mask & labeled_mask
data.val_mask = data.val_mask & labeled_mask
data.test_mask = data.test_mask & labeled_mask

# Separate indices of positive and negative samples among labeled nodes
labeled_indices = torch.where(data.y != -1)[0]
positive_indices = labeled_indices[data.y[labeled_indices] == 1]
negative_indices = labeled_indices[data.y[labeled_indices] == 0]
min_class_size = min(len(positive_indices), len(negative_indices))
balanced_positive_indices = positive_indices[torch.randperm(len(positive_indices))[:min_class_size]]
balanced_negative_indices = negative_indices[torch.randperm(len(negative_indices))[:min_class_size]]
balanced_indices = torch.cat((balanced_positive_indices, balanced_negative_indices))
balanced_indices = balanced_indices[torch.randperm(len(balanced_indices))]
train_size = int(0.8 * len(balanced_indices))  # 80% for training
val_size = int(0.1 * len(balanced_indices))    # 10% for validation
data.train_mask[:] = False
data.val_mask[:] = False
data.test_mask[:] = False
data.train_mask[balanced_indices[:train_size]] = True
data.val_mask[balanced_indices[train_size:train_size + val_size]] = True
data.test_mask[balanced_indices[train_size + val_size:]] = True


class GeneGNNBinary(Module):
    def __init__(self, num_node_features, embedding_dim):
        super(GeneGNNBinary, self).__init__()
        self.embeddings = nn.Embedding(embedding_dim=embedding_dim, num_embeddings=num_node_features)
        torch.nn.init.xavier_uniform_(self.embeddings.weight.data)
        self.conv1 = GCNConv(embedding_dim, 16)  # Adjust input dimension
        self.conv2 = GCNConv(16, 1)  # Assuming binary classification
        self.sigmoid = nn.Sigmoid()
    def forward(self, node_idx, edge_index, edge_attr):
        node_embeddings = self.embeddings(node_idx)
        x = F.relu(self.conv1(node_embeddings, edge_index, edge_weight=edge_attr.view(-1)))
        x = self.conv2(x, edge_index, edge_weight=edge_attr.view(-1))
        x = self.sigmoid(x).squeeze()
        return x

from torch_geometric.nn import GATConv

class GAT(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim, feature_dim):
        super().__init__()
        self.embeddings = nn.Embedding(embedding_dim=embedding_dim, num_embeddings=num_node_features)
        torch.nn.init.xavier_uniform_(self.embeddings.weight.data)
        self.conv1 = GATConv(embedding_dim + feature_dim, 16, heads=8, dropout=0.6)
        #self.conv2 = GATConv(16 * 8, 1, heads=1, dropout=0.6)
    def forward(self, node_idx, edge_index, edge_attr, node_features):
        node_embeddings = self.embeddings(node_idx)
        x = torch.cat([node_features, node_embeddings], dim=1)
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = torch.sigmoid(x)
        return x

# Assuming you have the dataset ready
feature_dim = data.x.shape[1]  # Number of features for each node
embedding_dim = 512  # Dimensionality of the embeddings
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#device = 'cpu'
# Generate a tensor of node indices [0, 1, ..., num_nodes-1]
node_idx = torch.arange(0, data.num_nodes, dtype=torch.long)

model = GeneGNNBinary(num_node_features=data.x.shape[0], embedding_dim=embedding_dim).to(device)
#model = GAT(num_node_features=data.x.shape[0], embedding_dim=embedding_dim, feature_dim=feature_dim).to(device)
# positive_weight = torch.tensor([torch.sum(essentiality_binary == 0) / torch.sum(essentiality_binary == 1)])
# criterion = nn.BCEWithLogitsLoss(pos_weight=positive_weight.to(device))
data = data.to(device)
model = model.to(device)
node_idx = node_idx.to(device)

optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
criterion = nn.BCELoss()

# Training loop
model.train()
for epoch in range(100):
    optimizer.zero_grad()
    out = model(node_idx, data.edge_index, data.edge_attr)
    #loss = criterion(out[data.train_mask], data.y[data.train_mask].unsqueeze(1).float())
    loss = criterion(out[data.train_mask], data.y[data.train_mask].float())
    loss.backward()
    optimizer.step()
    if epoch % 10 == 0:
        print(f'Epoch {epoch}: Training loss = {loss.item()}')

# Evaluation
model.eval()
threshold = 0.5
with torch.no_grad():
    out = model(node_idx, data.edge_index, data.edge_attr)
    preds = (out > threshold).float()
    #correct = (preds[data.test_mask][:, 0] == data.y[data.test_mask]).float().sum()
    correct = (preds[data.test_mask] == data.y[data.test_mask]).float().sum()
    accuracy = correct / int(data.test_mask.sum())
    print(f'Test Accuracy: {accuracy.item() * 100:.2f}%')
