import torch
from torch.nn import Linear, Module
import torch.nn.functional as F
from torch_geometric.nn import GCNConv
from torch_geometric.data import Data
import torch.nn as nn

import torch
import io_manager as iom
import configs as configs
import preprocess.gnn_dataprep as gnn_dp
import pandas as pd
import preprocess.nature_ppi_preprocess as npp

# dataset = Planetoid(root='data/Planetoid', name='Cora', transform=NormalizeFeatures())
# data = dataset[0]  # Get the first graph object.
# uni, count = torch.unique(data.y, return_counts=True)


gene_protein_df = iom.parse_proteins_seq(configs.pf_protein_seqs)
ppi_df = pd.read_csv(configs.pf_ppi, sep='\t')
ppi_df = npp.get_nature_ppi()
ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
annot_df = iom.parse_annot(configs.annot)
atac_df = iom.parse_atac_peak(configs.atac_peak_add)
assembly = iom.parse_fasta(configs.assembly)

data = gnn_dp.build_data(gene_protein_df, ppi_df, ess_g_df, annot_df, atac_df, assembly)

torch.manual_seed(0)  # Use a fixed seed for reproducibility
num_nodes = data.x.shape[0]
indices = torch.randperm(num_nodes)
train_size = int(0.8 * num_nodes)  # 80% for training
val_size = int(0.1 * num_nodes)    # 10% for validation
data.train_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.val_mask = torch.zeros(num_nodes, dtype=torch.bool)  # Validation mask
data.test_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.train_mask[indices[:train_size]] = True
data.val_mask[indices[train_size:train_size + val_size]] = True  # Validation mask
data.test_mask[indices[train_size + val_size:]] = True

class GeneGNNBinary(Module):
    def __init__(self, num_node_features, embedding_dim, feature_dim):
        super(GeneGNNBinary, self).__init__()
        self.embeddings = nn.Embedding(embedding_dim=embedding_dim, num_embeddings=num_node_features)
        torch.nn.init.xavier_uniform_(self.embeddings.weight.data)
        self.conv1 = GCNConv(embedding_dim + feature_dim, 16)  # Adjust input dimension
        self.conv2 = GCNConv(16, 1)  # Assuming binary classification
        self.sigmoid = nn.Sigmoid()
    def forward(self, node_idx, edge_index, edge_attr, node_features):
        node_embeddings = self.embeddings(node_idx)
        x = torch.cat([node_features, node_embeddings * 0], dim=1)
        x = F.relu(self.conv1(x, edge_index, edge_weight=edge_attr.view(-1)))
        x = self.conv2(x, edge_index, edge_weight=edge_attr.view(-1))
        x = self.sigmoid(x).squeeze()
        return x

from torch_geometric.nn import GATConv

class GAT(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim, feature_dim):
        super().__init__()
        self.embeddings = nn.Embedding(embedding_dim=embedding_dim, num_embeddings=num_node_features)
        torch.nn.init.xavier_uniform_(self.embeddings.weight.data)
        self.conv1 = GATConv(embedding_dim + feature_dim, 16, heads=8, dropout=0.6)
        #self.conv2 = GATConv(16 * 8, 1, heads=1, dropout=0.6)
    def forward(self, node_idx, edge_index, edge_attr, node_features):
        node_embeddings = self.embeddings(node_idx)
        x = torch.cat([node_features, node_embeddings], dim=1)
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = torch.sigmoid(x)
        return x

# Assuming you have the dataset ready
feature_dim = data.x.shape[1]  # Number of features for each node
embedding_dim = 512  # Dimensionality of the embeddings
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = 'cpu'
# Generate a tensor of node indices [0, 1, ..., num_nodes-1]
node_idx = torch.arange(0, data.num_nodes, dtype=torch.long)

model = GeneGNNBinary(num_node_features=data.x.shape[0], embedding_dim=embedding_dim, feature_dim=feature_dim).to(device)
#model = GAT(num_node_features=data.x.shape[0], embedding_dim=embedding_dim, feature_dim=feature_dim).to(device)
data = data.to(device)
node_idx = node_idx.to(device)

optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
criterion = nn.BCELoss()

# Training loop
model.train()
for epoch in range(4000):
    optimizer.zero_grad()
    out = model(node_idx, data.edge_index, data.edge_attr, data.x)
    #loss = criterion(out[data.train_mask], data.y[data.train_mask].unsqueeze(1).float())
    loss = criterion(out[data.train_mask], data.y[data.train_mask].float())
    loss.backward()
    optimizer.step()
    if epoch % 10 == 0:
        print(f'Epoch {epoch}: Training loss = {loss.item()}')

# Evaluation
model.eval()
threshold = 0.5
with torch.no_grad():
    out = model(node_idx, data.edge_index, data.edge_attr, data.x)
    preds = (out > threshold).float()
    #correct = (preds[data.test_mask][:, 0] == data.y[data.test_mask]).float().sum()
    correct = (preds[data.test_mask] == data.y[data.test_mask]).float().sum()
    accuracy = correct / int(data.test_mask.sum())
    print(f'Test Accuracy: {accuracy.item() * 100:.2f}%')
