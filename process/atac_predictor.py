import io_manager as im
import pandas as pd
import numpy as np
import configs as config
from calc import converter, utils
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
import preprocess.epigenetic_feature_prep as efp

annot_df = im.parse_annot(config.annot)
annot_df = annot_df[annot_df.type == 'gene']
atac_df = im.parse_atac_peak(config.atac_peak_add)
assembly = im.parse_fasta(config.assembly)
feature_seqs = efp.get_attac_seq_features(atac_df, assembly) + efp.get_h3k9ac_features(assembly, config.H3K9ac_peaks)

X_df = pd.DataFrame({'chr': annot_df['chr'],
                     'start': annot_df['start'],
                     'end': annot_df['end'],
                     'strand': annot_df['strand'],
                     'ID': annot_df['ID']})

bin_num = 10

for i, ftr_seq in enumerate(feature_seqs):
    X_df['feature_' + str(i)] = X_df.apply(lambda row: efp.calc_featers(row, ftr_seq, bin_num, reg='us'), axis=1)

ess_g_df = pd.read_csv(config.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
merged_df = X_df.merge(ess_g_df, left_on='ID', right_on='Gene_ID', how='inner')

X = pd.concat([pd.DataFrame(merged_df['feature_'+str(i)].tolist()) for i in range(len(feature_seqs))], axis=1)
Y = merged_df['MIS'].apply(lambda x: 1 if x > 0.5 else 0)

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)

model = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)
model.fit(x_train, y_train)
y_pred = model.predict(x_test)

accuracy_score(y_pred, y_test)

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
model = Sequential([
    Dense(16, activation='relu', input_shape=(40,)),  # First hidden layer with 64 neurons and ReLU activation
    Dense(4, activation='relu'),  # First hidden layer with 64 neurons and ReLU activation
    Dense(1, activation='sigmoid')  # Output layer with 1 neuron and sigmoid activation (for binary classification)
])
model.compile(optimizer=Adam(learning_rate=0.02), loss='binary_crossentropy', metrics=['accuracy'],)
history = model.fit(x_train, y_train, epochs=30, batch_size=32, validation_split=0.1)  # You can adjust epochs, batch_size, and validation_split as needed

y_pred = model.predict(x_test)
accuracy_score(y_pred[:, 0], y_test)

from sklearn.linear_model import LogisticRegression
model = LogisticRegression()
model.fit(x_train, y_train)
Y_pred = model.predict(x_test)

from scipy.stats import pearsonr
tmp = X.iloc[:, 3].tolist()
np.mean(tmp)
c, p = pearsonr(Y, tmp)
c, p
# -0.006107127230407376 0.6542023587100942



