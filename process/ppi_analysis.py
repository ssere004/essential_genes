import re
import pandas as pd
import configs
import io_manager as iom

gene_protein_df = iom.parse_proteins_seq(configs.pf_protein_seqs)
replacement_dict = gene_protein_df.set_index('protein')['gene'].to_dict()
ppi_df = pd.read_csv(configs.pf_ppi, sep='\t')
ppi_df['gene1'] = ppi_df['protein1'].map(replacement_dict)
ppi_df['gene2'] = ppi_df['protein2'].map(replacement_dict)
ppi_df = ppi_df.dropna().reset_index(drop=True)
ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
#PF3D7_0100100

ppi_df['combined_score'] = (ppi_df['combined_score'] - ppi_df['combined_score'].min())/\
                           (ppi_df['combined_score'].max() -ppi_df['combined_score'].min())
ppi_df['combined_score'] = ppi_df['combined_score'] + 0.00001

def calc_node_score(GID, gene_protein_df, ppi_df):
    proteins = gene_protein_df[gene_protein_df['gene'] == GID]['protein'].tolist()
    #return len(ppi_df[ppi_df['protein1'].isin(proteins)])
    return ppi_df[ppi_df['protein1'].isin(proteins)]['combined_score'].sum()

ess_g_df['ppi_score'] = ess_g_df.apply(lambda row: calc_node_score(row['Gene_ID'], gene_protein_df, ppi_df), axis=1)

ess_g_df['MIS'].corr(ess_g_df['ppi_score'])

# sub_df = ess_g_df[ess_g_df.ppi_score != 0]
# sub_df['MIS'].corr(sub_df['ppi_score'])
# len = 3455 len(ess_g_df) = 5399


