import torch
from torch_geometric.datasets import Planetoid
from torch_geometric.transforms import NormalizeFeatures
import io_manager as iom
import configs as configs
import preprocess.gnn_dataprep as gnn_dp
import pandas as pd
import preprocess.nature_ppi_preprocess as npp
from torch_geometric.nn import GCNConv, global_mean_pool
import torch.nn.functional as F
#
# dataset = Planetoid(root='data/Planetoid', name='Cora', transform=NormalizeFeatures())
# data = dataset[0]  # Get the first graph object.
# uni, count = torch.unique(data.y, return_counts=True)


gene_protein_df = iom.parse_proteins_seq(configs.pf_protein_seqs)
ppi_df = pd.read_csv(configs.pf_ppi, sep='\t')
#ppi_df = npp.get_nature_ppi()
ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
annot_df = iom.parse_annot(configs.annot)
atac_df = iom.parse_atac_peak(configs.atac_peak_add)
assembly = iom.parse_fasta(configs.assembly)

data = gnn_dp.build_data(gene_protein_df, ppi_df, ess_g_df, annot_df, atac_df, assembly, bin_num=10)


torch.manual_seed(0)  # Use a fixed seed for reproducibility
num_nodes = data.x.shape[0]
indices = torch.randperm(num_nodes)
train_size = int(0.8 * num_nodes)  # 80% for training
val_size = int(0.1 * num_nodes)    # 10% for validation
data.train_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.val_mask = torch.zeros(num_nodes, dtype=torch.bool)  # Validation mask
data.test_mask = torch.zeros(num_nodes, dtype=torch.bool)
data.train_mask[indices[:train_size]] = True
data.val_mask[indices[train_size:train_size + val_size]] = True  # Validation mask
data.test_mask[indices[train_size + val_size:]] = True

def train():
    model.train()
    optimizer.zero_grad()  # Clear gradients.
    #out = model(node_idx, data.edge_index, data.edge_attr)  # Perform a single forward pass.
    out = model(data.x, data.edge_index)  # Perform a single forward pass.
    loss = criterion(out[data.train_mask][:, 0], data.y[data.train_mask])  # Compute the loss solely based on the training nodes.
    val_loss = criterion(out[data.val_mask][:, 0], data.y[data.val_mask])
    loss.backward()  # Derive gradients.
    optimizer.step()  # Update parameters based on gradients.
    return loss, val_loss

def test():
    model.eval()
    #out = model(node_idx, data.edge_index, data.edge_attr)
    out = model(data.x, data.edge_index)
    pred = out.round()[:, 0]  # Use the class with highest probability.
    test_correct = pred[data.test_mask] == data.y[data.test_mask]  # Check against ground-truth labels.
    test_acc = int(test_correct.sum()) / int(data.test_mask.sum())  # Derive ratio of correct predictions.
    return test_acc


########################GAT#####################################################
from torch_geometric.nn import GATConv

class GAT(torch.nn.Module):
    def __init__(self, hidden_channels, num_features):
        super().__init__()
        self.conv1 = GATConv(num_features, hidden_channels, heads=12, dropout=0.6)
        self.conv2 = GATConv(hidden_channels * 12, 1, heads=1, dropout=0.6)
    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index)
        x = x.relu()
        x = self.conv2(x, edge_index)
        x = torch.sigmoid(x)
        return x

class GeneGNNBinary(torch.nn.Module):
    def __init__(self, embedding_dim):
        super(GeneGNNBinary, self).__init__()
        self.conv1 = GCNConv(embedding_dim, 16)
        self.conv2 = GCNConv(16, 1)  # Assuming output_dim is 1 for binary classification
        self.sigmoid = torch.nn.Sigmoid()
    def forward(self, x, edge_index):
        x = F.relu(self.conv1(x, edge_index))
        x = self.conv2(x, edge_index)
        x = self.sigmoid(x).squeeze()
        return x.unsqueeze(1)


class GeneGNNBinaryembedding(torch.nn.Module):
    def __init__(self, num_node_features, embedding_dim):
        super(GeneGNNBinaryembedding, self).__init__()
        self.embeddings = torch.nn.Embedding(embedding_dim=embedding_dim, num_embeddings=num_node_features)
        torch.nn.init.xavier_uniform_(self.embeddings.weight.data)
        self.conv1 = GCNConv(embedding_dim, 16)  # Adjust input dimension
        self.conv2 = GCNConv(16, 1)  # Assuming binary classification
        self.sigmoid = torch.nn.Sigmoid()
    def forward(self, node_idx, edge_index, edge_attr):
        node_embeddings = self.embeddings(node_idx.to(torch.int64))
        x = F.relu(self.conv1(node_embeddings, edge_index, edge_weight=edge_attr.view(-1)))
        x = self.conv2(x, edge_index, edge_weight=edge_attr.view(-1))
        x = self.sigmoid(x).squeeze()
        return x.unsqueeze(1)


model = GAT(hidden_channels=16, num_features=data.x.shape[1])
#model = GeneGNNBinary(embedding_dim=data.x.shape[1])
#node_idx = torch.arange(0, data.num_nodes, dtype=torch.long)
#model = GeneGNNBinaryembedding(num_node_features=data.x.shape[0], embedding_dim=20)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-4)
criterion = torch.nn.BCELoss()
for epoch in range(1, 80):
    loss, val_loss = train()
    print(f'Epoch: {epoch:03d}, Loss: {loss:.4f}, Validation loss: {val_loss:.4f}')

test_acc = test()
print(f'Test Accuracy: {test_acc:.4f}')


