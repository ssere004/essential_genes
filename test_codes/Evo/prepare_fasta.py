import configs
import io_manager as iom

# Please activate the hyena-dna env on Crispr

annot_df = iom.parse_annot(configs.annot)
annot_df = annot_df[annot_df.type == 'gene']
annot_df = annot_df.reset_index(drop=True)
assembly = iom.parse_fasta(configs.assembly)

llh_compare_ad = '/home/ssere004/projects/evo/scores/final_compare.tsv'

#len(annot_df) = 5777

for idx, row in annot_df.iterrows():
    with open('/home/ssere004/projects/evo/examples/wt_gene_{}.fasta'.format(idx), 'w') as file:
        file.write('>' + str(idx) + '\n')
        start, end = int(row['start']), int(row['end'])
        flanking_size = int(8000 - (end - start)/2)
        flanking_size = 0
        chr_size = len(assembly[row['chr']])
        file.write(str(assembly[row['chr']][max(start - flanking_size, 0): min(end + flanking_size, chr_size)]) + '\n')

for idx, row in annot_df.iterrows():
    with open('/home/ssere004/projects/evo/examples/premature_gene_{}.fasta'.format(idx), 'w') as file:
        file.write('>' + str(idx) + '\n')
        start, end = int(row['start']), int(row['end'])
        flanking_size = int(8000 - (end - start)/2)
        flanking_size = 0
        res = str(assembly[row['chr']][max(start - flanking_size, 0): start + 100])
        res += 'TAG'
        res += str(assembly[row['chr']][start + 100: min(end + flanking_size, chr_size)])
        file.write(res + '\n')


# python -m scripts.score --input-fasta examples/wt_gene_.fasta --output-tsv scores_wt.tsv --model-name evo-1-131k-base --device cuda:0
# python -m scripts.score --input-fasta examples/premature_gene_66.fasta --output-tsv scores_premature.tsv --model-name evo-1-131k-base --device cuda:0

with open('/home/ssere004/projects/evo/run.sh', 'w') as file:
    for i in range(5777):
        file.write('python -m scripts.score --input-fasta examples/wt_gene_{}.fasta --output-tsv ./scores/scores_wt_{}.tsv --model-name evo-1-131k-base --device cuda:0'.format(str(i), str(i)) + '\n')
        file.write('python -m scripts.score --input-fasta examples/premature_gene_{}.fasta --output-tsv ./scores/scores_premature_{}.tsv --model-name evo-1-131k-base --device cuda:0'.format(str(i), str(i)) + '\n')



import pandas as pd

wt_scores = []
pm_scores = []
for i in range(166):
    df_wt = pd.read_csv('scores_wt_{}.tsv'.format(i), sep='\t')
    df_pm = pd.read_csv('scores_premature_{}.tsv'.format(i), sep='\t')
    if len(df_wt) == 1 and len(df_pm) == 1:
        wt_scores.append(df_wt.iloc[0]['scores'])
        pm_scores.append(df_pm.iloc[0]['scores'])

pd.DataFrame({'wt_lh':wt_scores, 'pm_lh':pm_scores}).to_csv('final_compare.tsv', index=False, sep='\t')

ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
