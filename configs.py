
atac_peak_add = '/data/saleh/Species/PF/ATAC/atac_peaks.txt'
annot = '/data/saleh/Species/PF/PlasmoDB-28/PlasmoDB-28_Pfalciparum3D7.gff'
assembly = '/data/saleh/Species/PF/PlasmoDB-28/PlasmoDB-28_Pfalciparum3D7_Genome.fasta'
essential_genes = '/data/saleh/Species/PF/pf_essential_genes.text'
pf_protein_seqs = '/data/saleh/Species/PF/ppi/UP000001450_36329.fasta'
pf_ppi = '/data/saleh/Species/PF/ppi/clean_ppi.tsv'

H3K9ac_peaks = {
    '10h': '/data/saleh/Species/PF/H3K9ac/macs2_callpeak_10hpi.bed',
    '20h': '/data/saleh/Species/PF/H3K9ac/macs2_callpeak_20hpi.bed',
    '30h': '/data/saleh/Species/PF/H3K9ac/macs2_callpeak_30hpi.bed',
    '40h': '/data/saleh/Species/PF/H3K9ac/macs2_callpeak_40hpi.bed'
}

yeast_config = {
    'assembly': '/home/ssere004/Organisms/Saccharomyces/assembely/S288C_reference_genome_R40-1-1_20040709/S288C_reference_sequence_R40-1-1_20040709.fsa',
    'annot': '/home/ssere004/Organisms/Saccharomyces/assembely/S288C_reference_genome_R40-1-1_20040709/saccharomyces_cerevisiae_R40-1-1_20040710.gff',
    'ppi': '/home/ssere004/Organisms/Saccharomyces/ppi/ppi_processed.csv',
    'ess_g': '/home/ssere004/Organisms/Saccharomyces/essential_genes/Saccharomyces-cerevisiae-W303_genes.csv',
    'gene_name_converter': '/home/ssere004/Organisms/Saccharomyces/gene_name_conversion.tsv'
}


