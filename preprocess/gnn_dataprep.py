import configs
import io_manager as iom
import pandas as pd
import configs
from calc import converter
import torch
from torch_geometric.data import Data, DataLoader
from torch_geometric.nn import GCNConv, global_mean_pool
import pandas as pd
import numpy as np
import preprocess.epigenetic_feature_prep as efp

def build_data(gene_protein_df, ppi_df, ess_g_df, annot_df, atac_df, assembly, bin_num=5):
    replacement_dict = gene_protein_df.set_index('protein')['gene'].to_dict()
    if 'protein1' in ppi_df.columns:
        ppi_df['gene1'] = ppi_df['protein1'].map(replacement_dict)
        ppi_df['gene2'] = ppi_df['protein2'].map(replacement_dict)
        ppi_df = ppi_df.dropna().reset_index(drop=True)
    annot_df = annot_df[annot_df.type == 'gene']
    feature_seqs = efp.get_attac_seq_features(atac_df, assembly) + efp.get_h3k9ac_features(assembly, configs.H3K9ac_peaks)
    X_df = pd.DataFrame({'chr': annot_df['chr'],
                         'start': annot_df['start'],
                         'end': annot_df['end'],
                         'strand': annot_df['strand'],
                         'ID': annot_df['ID']})
    for i, ftr_seq in enumerate(feature_seqs):
        X_df['feature_' + str(i)] = X_df.apply(lambda row: efp.calc_featers(row, ftr_seq, bin_num, reg='us'), axis=1)
    feature_columns = [col for col in X_df.columns if 'feature_' in col]
    gene_features_dict = X_df.set_index('ID')[feature_columns].apply(lambda row: [item for sublist in row for item in sublist], axis=1).to_dict()
    ppi_df['combined_score'] = (ppi_df['combined_score'] - ppi_df['combined_score'].min())/(ppi_df['combined_score'].max() - ppi_df['combined_score'].min())
    #making a dictionary that converts gene names to their index, The size of whole dictionary is the number of all available genes
    gene_to_idx = {gene: idx for idx, gene in enumerate(np.unique(ppi_df[['gene1', 'gene2']].values.flatten()))}
    #making a tensor with shape of [2, edge_number], that for each [:,i] it contains two gene indexes (computed in the line above) that they are connected in the graph
    edge_index = torch.tensor([[gene_to_idx[gene1], gene_to_idx[gene2]] for gene1, gene2 in zip(ppi_df['gene1'], ppi_df['gene2'])], dtype=torch.long).t().contiguous()
    #Making the edge features basd on the ppi_df, see that the zip(ppi_df['gene1'], ppi_df['gene2']) in the line above is in the same order of ppi_df['combined_score'].values in the line below
    edge_attr = torch.tensor(ppi_df['combined_score'].values, dtype=torch.float).unsqueeze(1)
    #edge_attr[:, 0] = 1
    # Initialize node_features tensor with zeros
    node_features = torch.zeros(len(gene_to_idx), len(feature_columns) * bin_num)
    # Assign the specific vector to each node
    for gene, idx in gene_to_idx.items():
        if gene in gene_features_dict:
            node_features[idx] = torch.tensor(gene_features_dict[gene])
    num_genes = len(gene_to_idx)
    essentiality_reg = torch.zeros(num_genes)
    essentiality_binary = torch.zeros(num_genes)
    threshold = 0.5
    for _, row in ess_g_df.iterrows():
        gene = row['Gene_ID']
        mis_value = row['MIS']
        if gene in gene_to_idx.keys():
            index = gene_to_idx[gene]
            essentiality_reg[index] = mis_value
            essentiality_binary[index] = 1 if mis_value > threshold else 0
    #make dummy dataset easy to predict
    # for idx in range(len(essentiality_binary)):
    #     if essentiality_binary[idx] == 1:
    #         node_features[idx] = (node_features[idx] * 0) + 1
    #     else:
    #         node_features[idx] = node_features[idx] * 0 + 0.01
    data = Data(x=node_features, edge_index=edge_index, edge_attr=edge_attr, y=essentiality_binary)
    return data
