import pandas as pd
import configs

def get_nature_ppi():
    df_ids = pd.read_csv('/data/saleh/Species/PF/ppi/gene_ID_map.txt', names=['gid', 'pid', 'num', 'gid_o'], sep='\t')
    df_ppi = pd.read_csv('/data/saleh/Species/PF/ppi/nature_ppi.txt', sep='\t')
    df_ppi = df_ppi.rename(columns={'Bait ORF name*': 'gene1', 'Prey ORF name*': 'gene2', 'Cummulative number of times this interaction was observed': 'combined_score', 'Number of independent yeast two-hybrid searches that found this interaction':'y2h'})
    df_ppi = df_ppi[['gene1', 'gene2', 'combined_score', 'y2h']]
    replacement_dict = df_ids.set_index('gid_o')['gid'].to_dict()
    df_ppi['gene1'] = df_ppi['gene1'].map(replacement_dict)
    df_ppi['gene2'] = df_ppi['gene2'].map(replacement_dict)
    df_ppi = df_ppi.dropna().reset_index(drop=True)
    #df_ppi.to_csv('y2h_nature_ppi_processed.csv', sep='\t', index=False)
    return df_ppi

def calc_corr():
    df_ppi = get_nature_ppi()
    #df_ppi.to_csv('y2h_nature_ppi_processed.csv', sep='\t', index=False)
    def calc_node_score(GID, ppi_df):
        return ppi_df[ppi_df['gene1'] == GID]['interaction_num'].sum()
        #return len(ppi_df[ppi_df['Bait'] == GID])
    ess_g_df = pd.read_csv(configs.essential_genes, sep='\t')[['Gene_ID', 'MIS']]
    ess_g_df['ppi_score'] = ess_g_df.apply(lambda row: calc_node_score(row['Gene_ID'], df_ppi), axis=1)
    ess_g_df['MIS'].corr(ess_g_df['ppi_score'])
    sub_df = ess_g_df[ess_g_df.ppi_score != 0]
    sub_df['MIS'].corr(sub_df['ppi_score'])
    # len = 3455 len(ess_g_df) = 5399


