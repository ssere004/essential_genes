import pandas as pd
import numpy as np
from calc import converter, utils


def get_attac_seq_features(atac_df, assembly):
    feature_seqs = []
    for ftr in ['T10', 'T20', 'T30', 'T40']:
        df = atac_df[['chr', 'start', 'end', ftr]].rename(columns={'chr': 'chr', 'start': 'start', 'end': 'end', ftr: 'value'})
        feature_seqs.append(converter.make_interval_seq_dic(assembly, df))
    return feature_seqs

def get_h3k9ac_features(assembly, H3K9ac_peaks):
    feature_seqs = []
    for stge in H3K9ac_peaks.keys():
        df = pd.read_csv(H3K9ac_peaks[stge], header=None, names=['chr', 'start', 'end', 'peak_id', 'value'], sep='\t')
        for i in range(1, 15):
            if i < 10:
                df.replace('chr'+str(i), 'pf3d7_0{}_v3'.format(i), inplace=True)
            else:
                df.replace('chr'+str(i), 'pf3d7_{}_v3'.format(i), inplace=True)
        feature_seqs.append(converter.make_interval_seq_dic(assembly, df))
    return feature_seqs

def calc_featers(row, feature_seq, bin_num, reg='gb', flanking=500):
    cord_dic ={}
    cord_dic['gb'] = utils.make_bins(row['start'], row['end'], bin_num)
    if row['strand'] == '+':
        cord_dic['us'] = utils.make_bins(row['start'] - flanking, row['start'], bin_num)
        cord_dic['ds'] = utils.make_bins(row['end'], row['end'] + flanking, bin_num)
    else:
        cord_dic['us'] = utils.make_bins(row['end'], row['end'] + flanking, bin_num)
        cord_dic['ds'] = utils.make_bins(row['start'] - flanking, row['start'], bin_num)
    bin_coords = cord_dic[reg]
    if row['strand'] == '+':
        return [np.mean(feature_seq[row['chr']][bin_coords[i]: bin_coords[i + 1]]) for i in range(len(bin_coords)-1)]
    else:
        return [np.mean(feature_seq[row['chr']][bin_coords[i]: bin_coords[i + 1]]) for i in range(len(bin_coords)-1)][::-1][::-1]
