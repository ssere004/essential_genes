

# This function gets start and end of a gene and returns the coordinates of the split locations including start and end of the gene
def make_bins(start, end, bin_num):
    bin_size = int((end - start)/bin_num)
    residue = (end - start) % bin_num
    f_part = [start+i*(bin_size+1) for i in range(residue+1)]
    s_part = [start+(residue-1)*(bin_size+1)+1 + i*bin_size for i in range(2, bin_num - residue+1)]
    return f_part+s_part+[end]
