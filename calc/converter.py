import numpy as np
import pandas as pd

def interval_filler(df, l):
    arr = np.zeros(l, dtype=float)
    df.apply(lambda row: fill_interval(arr, row['start'], row['end'], row['value']), axis=1)
    return arr

def fill_interval(arr, start, stop, value):
    arr[start:stop] = float(value)

# Makes a seqeuence the same as seq_dic and assign 1 to the intervals of df
def make_interval_seq_dic(seq_dic, df):
    assert 'start' in df.columns and 'end' in df.columns and 'value' in df.columns
    res_seq_dic = {}
    for key in seq_dic.keys():
        res_seq_dic[key] = interval_filler(df[df['chr'] == key], len(seq_dic[key]))
    return res_seq_dic
